package com.example.word_lookup_mini_project;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolder extends RecyclerView.ViewHolder {
    private TextView textViewTranslated;
    public ViewHolder(@NonNull View itemView) {
        super(itemView);

        textViewTranslated = itemView.findViewById(R.id.translated);
    }

    public TextView getTextViewTranslated(){
        return textViewTranslated;
    }
}
