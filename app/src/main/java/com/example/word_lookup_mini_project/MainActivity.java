package com.example.word_lookup_mini_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<String> kiswahiliWords, englishWords;
    private Button buttonExit;
    private Button buttonTranslate;
    private EditText editTextEnterWord;
    private TextView textViewMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        kiswahiliWords = new ArrayList<>();
        englishWords = new ArrayList<>();
        buttonExit = findViewById(R.id.buttonExit);
        buttonTranslate = findViewById(R.id.buttonTranslate);
        editTextEnterWord = findViewById(R.id.editTextEnterWord);
        textViewMessage = findViewById(R.id.textViewMessage);

        loadCsvFile();

        buttonTranslate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                translateText();
            }
        });

        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                homeIntent.addCategory( Intent.CATEGORY_HOME );
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
            }
        });
    }

    private void loadCsvFile(){
        InputStream inputStream = getResources().openRawResource(R.raw.words);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try {
            String line;
            while ((line = reader.readLine()) != null){
                try {
                    String[] words = line.split(",");
                    if (words.length == 2){
                        kiswahiliWords.add(words[0]);
                        englishWords.add(words[1]);
                    }
                }catch (Exception e){
                    Log.e("Error", e.toString());
                }
            }
        }
        catch (IOException io){
            Log.e("Error", io.toString());
        }
    }

    private void translateText(){
        String word = editTextEnterWord.getText().toString().trim();
        String translationMessage = "";
        ArrayList<String> words = new ArrayList<>();

        if (englishWords.contains(word)){
            translationMessage = "Hii ndio tafsiri la neno:" + " " + word;

            for (int i=0; i<englishWords.size(); i++){
                if (englishWords.get(i).equals(word)){
                    words.add(kiswahiliWords.get(i));
                }
            }
        }

        else if (kiswahiliWords.contains(word)){
            translationMessage = "The translations for the word" + " " + word;
            for(int i=0; i<kiswahiliWords.size(); i++){
                if (kiswahiliWords.get(i).equals(word)){
                    words.add(englishWords.get(i));
                }
            }
        }

        else {
            translationMessage = "The word isn't in the dictionary";
        }

        textViewMessage.setText(translationMessage);

        Adapter adapter = new Adapter(words,this);
        RecyclerView view = findViewById(R.id.recyclerView);
        view.setAdapter(adapter);
        view.setLayoutManager(new LinearLayoutManager(this));

    }
}
